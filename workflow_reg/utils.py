import io
import logging
import os
import zipfile

import requests

from workflow_registry.settings import all_properties

logger = logging.getLogger(__name__)
user_identifier_sep = "#dare#"


def collect_scripts(script_names, files):
    """
    Utility method to collect the given scripts into a dictionary with key their relevant names

    Args
        | script_names (list): a list of the provided script names
        | files (dict): key-value pairs with the content of each script

    Returns
        dict: key-value pairs of script names and their content
    """
    scripts = {}
    if script_names:
        for script_name in script_names:
            script = files[script_name]
            scripts[script_name] = script
    return scripts


def get_files(file_names):
    """
    Files to download

    Args
        file_names (dict): name and content of files to download

    Returns
        bytes: the content to download
    """
    outfile = io.BytesIO()
    with zipfile.ZipFile(outfile, 'w') as zf:
        for n, f in file_names.items():
            zf.writestr(n, f)
    return outfile.getvalue()
