from os.path import join

from django.test import TestCase

from workflow_reg.services import DockerEnvService
from workflow_registry.settings import BASE_DIR

workflow_client_folder = join(BASE_DIR, "workflow_client")
path_to_docker_files = join(workflow_client_folder, "docker_env")


# Create your tests here.
class DockerEnvTest(TestCase):
    docker_env_service = DockerEnvService()
    name = "test"
    initial_tag = "v1.0"
    update_tag = "v1.1"
    user_identifier = "user"
    script_names = ["entrypoint.sh"]

    with open(join(path_to_docker_files, "Dockerfile"), "rb") as df:
        dockerfile = df.read()
    scripts = {}
    for script_name in script_names:
        with open(join(path_to_docker_files, script_name), "rb") as s:
            script = s.read()
        scripts[script_name] = script

    def test_create_docker(self):
        docker_env = self.docker_env_service.create_docker_env(name=self.name, tag=self.initial_tag,
                                                               dockerfile=self.dockerfile,
                                                               user_identifier=self.user_identifier,
                                                               scripts=self.scripts)
        docker_env_retrieved = self.docker_env_service.get_unique_docker_env(name=self.name, tag=self.initial_tag)
        self.assertEqual(docker_env.name, docker_env_retrieved.name)
