from django.contrib import admin
from workflow_reg.models import DockerEnv, DockerScript, Workflow, WorkflowPart

# Register your models here.
admin.register(DockerEnv)
admin.register(DockerScript)
admin.register(Workflow)
admin.register(WorkflowPart)
