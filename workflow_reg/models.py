from django.db import models


class AbstractClass(models.Model):
    """
    Abstract class containing columns appearing in all tables. Keeps information on who and when created or
    updated a DB entry. All following models extend this class
    """
    class Meta:
        abstract = True

    creation_date = models.DateTimeField()
    created_by = models.CharField(max_length=2000)
    modification_date = models.DateTimeField()
    modified_by = models.CharField(max_length=2000)


# Create your models here.
class DockerEnv(AbstractClass):
    """
    Represents a docker container. Each DB entry is identified as unique based on the docker name and tag.

    The user initially gives a name, tag, dockerfile and if he/she wishes additional docker scripts (if used inside the
    Dockerfile). Once a container is registered, the platform's admin checks the files and builds a docker image.
    This image should be publicly available and the url field should be updated accordingly once the image is built and
    available.
    """
    class Meta:
        db_table = "docker_env"
        unique_together = ("name", "tag",)

    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=300)
    tag = models.CharField(max_length=300)
    url = models.CharField(max_length=4000)
    user_identifier = models.CharField(max_length=4000)
    dockerfile = models.TextField(null=False, blank=False)

    def get_scripts(self):
        """
        Method to retrieve all the scripts associated with a specific docker container

        Returns
            list: list of scripts associated with the specific docker container object instance.
        """
        return DockerScript.objects.filter(docker_env=self)

    def get_workflows(self):
        """
        Method to retrieve all the workflows associated with a specific docker container

        Returns
            list: list of workflows associated with the specific docker container object instance.
        """
        return Workflow.objects.filter(docker_env=self)


class DockerScript(AbstractClass):
    """
    This class is associated with a DockerEnv DB entry. Represents an additional script used by a Dockerfile.

    Instances of this class can be created either with a DockerEnv creation or separately, where one by one the
    scripts should be registered in the DB. The script is characterized based on the associated DockerEnv and its own
    name.
    """
    class Meta:
        db_table = "docker_scripts"
        unique_together = ("docker_env", "file_name")

    id = models.AutoField(primary_key=True, unique=True)
    file_name = models.CharField(max_length=1000)
    docker_env = models.ForeignKey(DockerEnv, related_name="scripts", on_delete=models.CASCADE)
    script = models.TextField(null=False, blank=False)


class Workflow(AbstractClass):
    """
    Workflow class represents a CWL workflow. In this table, only the CWL characterized as workflows are registered.
    Each workflow is unique based on its name and version. The user should provide a name, version, the name of the
    spec yaml file, the two files (i.e. the workflow file and the spec file) as well as a related DockerEnv where
    the workflow will be executed.

    It is possible to provide the CWL files used inside the CWL workflow (i.e. those that contain a Command Line
    instead of a workflow). These files are represented by the WorkflowPart class. However, if the additional CWL
    files are not provided upon a Workflow object creation, users can register them afterwards one by one.
    """
    class Meta:
        db_table = "workflows"
        unique_together = ("name", "version")

    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=300)
    version = models.CharField(max_length=300)
    workflow = models.TextField(null=False, blank=False)
    spec_name = models.CharField(max_length=300)
    spec = models.TextField(null=False, blank=False)
    user_identifier = models.CharField(max_length=4000)
    docker_env = models.ForeignKey(DockerEnv, related_name="workflows", on_delete=models.CASCADE)

    def get_workflow_parts(self):
        return WorkflowPart.objects.filter(workflow=self)


class WorkflowPart(AbstractClass):
    """
    This class represents a CWL file, part of a CWL workflow (kind CommandLineTool for example). Each object should be
    associated with a Workflow instance and should have a name, version and a spec yaml file.

    CWL workflow parts can be registered at the same time with their parent workflow or afterwards using the
    WorkflowPartView
    """
    class Meta:
        db_table = "workflow_parts"
        unique_together = ("workflow", "name", "version")

    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=300)
    version = models.CharField(max_length=300)
    spec_name = models.CharField(max_length=300)
    workflow = models.ForeignKey(Workflow, related_name="workflow_parts", on_delete=models.CASCADE)
    script = models.TextField(null=False, blank=False)
    spec = models.TextField(null=False, blank=False)
