from django.contrib.auth.models import User
from rest_framework import serializers
from workflow_reg.models import DockerScript, DockerEnv, Workflow, WorkflowPart


class WorkflowPartSerializer(serializers.ModelSerializer):
    """
    Serializer associated with the WorkflowPart model, representing a CWL workflow part file.
    """

    class Meta:
        model = WorkflowPart
        fields = ["id", "name", "version", "spec_name", "workflow", "script", "spec", "creation_date", "created_by",
                  "modification_date", "modified_by"]
        depth = 1


class WorkflowSerialiser(serializers.ModelSerializer):
    """
    Serializer class for Workflow instances, representing a CWL workflow
    """
    workflow_parts = WorkflowPartSerializer(source="get_workflow_parts", many=True, read_only=True)

    class Meta:
        model = Workflow
        fields = ["id", "name", "version", "workflow", "spec_name", "spec", "user_identifier", "docker_env",
                  "creation_date", "created_by", "modification_date", "modified_by", "workflow_parts"]
        depth = 2


class DockerScriptSerializer(serializers.ModelSerializer):
    """
    Serializer associated with the DockerScript class, which represents an additional script used by a Dockerfile
    """

    class Meta:
        model = DockerScript
        fields = ["id", "docker_env", "file_name", "script", "creation_date", "created_by", "modification_date",
                  "modified_by"]
        depth = 1


class DockerEnvSerializer(serializers.ModelSerializer):
    """
    Serializer associated with a DockerEnv instance, which represents a docker container
    """
    scripts = DockerScriptSerializer(source='get_scripts', many=True, read_only=True)
    workflows = WorkflowSerialiser(source='get_workflows', many=True, read_only=True)

    class Meta:
        model = DockerEnv
        fields = ["id", "name", "tag", "url", "user_identifier", "dockerfile", "creation_date", "created_by",
                  "modification_date", "modified_by", "scripts", "workflows"]
        depth = 1


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email"]
