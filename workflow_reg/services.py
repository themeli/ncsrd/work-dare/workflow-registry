from django.utils import timezone

from workflow_reg.models import DockerEnv, DockerScript, Workflow, WorkflowPart
from workflow_reg import utils


class DockerEnvService:
    """
    Backend methods for DockerEnv django model
    """

    def create_docker_env(self, name, tag, dockerfile, user_identifier, scripts=None):
        """
        Method used to create a DockerEnv. If scripts parameter is provided, the relevant DockerScript objects are
        also registered in the DB.

        Args
            | name (str): the name of the docker container
            | tag (str): the tag of the docker container
            | dockerfile (str): the content of the Dockerfile describing the docker container
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak
            | scripts (dict): key-value pairs of the script names and the relevant content
        Returns
            DockerEnv: the registered DockerEnv
        """
        docker_env = self.__save(name=name, tag=tag, dockerfile=dockerfile, user_identifier=user_identifier)
        docker_script_service = DockerScriptService()
        if scripts:
            for script_name, script in scripts.items():
                docker_script_service.create_script(script_name, name, tag, script, user_identifier)
        return docker_env

    def update_docker_env(self, update_fields, name, tag, user_identifier, docker_file=None):
        """
        Method to update a DockerEnv instance. It is possible to also update the relevant Dockerfile

        Args
            | update_fields (dict): containes the field names and their updated values
            | name (str): the name of the docker container
            | tag (str): the current tag of the docker container
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak
            | docker_file (str): the content of the Dockerfile
        Returns
            DockerEnv: the updated DockerEnv instance
        """
        if docker_file:
            update_fields["dockerfile"] = docker_file
        docker_env = self.get_unique_docker_env(name=name, tag=tag)
        update_fields["modification_date"] = timezone.now()
        update_fields["modified_by"] = user_identifier
        docker_env.__dict__.update(update_fields)
        docker_env.save(update_fields=update_fields.keys())
        return docker_env

    def delete_docker_env(self, name, tag):
        """
        Method to delete a DockerEnv instance from the DB

        Args
            | name (str): the name of the docker container
            | tag (str): the tag of the docker container
        """
        docker_env = self.get_unique_docker_env(name=name, tag=tag)
        docker_env.delete()

    def download_docker_env(self, name, tag):
        """
        Method to download a Dockerfile and its associated scripts

        Args
            | name (str): the name of the docker container
            | tag (str): the tag of the docker container

        Returns
            bytes: the files to download
        """
        docker_script_service = DockerScriptService()
        docker_env = self.get_unique_docker_env(name=name, tag=tag)
        scripts = docker_script_service.filter_by_docker_env(docker_env=docker_env)
        env_files = {"Dockerfile": docker_env.dockerfile}
        if scripts:
            for script in scripts:
                env_files[script.file_name] = script.script
        return utils.get_files(env_files)

    @staticmethod
    def get_unique_docker_env(name, tag):
        """
        Method to retrieve a unique docker container based on its name and tag

        Args
            | name (str): the name of the docker container
            | tag (str): the tag of the docker container

        Returns
            DockerEnv: the retrieved docker container
        """
        return DockerEnv.objects.get(name=name, tag=tag)

    @staticmethod
    def filter_docker_env_by_user(user_identifier):
        """
        Method to retrieve all the docker containers registered by a user

        Args
            user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            list: all the DockerEnv created by a specific user
        """
        return DockerEnv.objects.filter(user_identifier=user_identifier)

    @staticmethod
    def __save(name, tag, dockerfile, user_identifier):
        """
        Private method used to save a DockerEnv instance

        Args
            | name (str): the name of the docker container
            | tag (str): the tag of the docker container
            | dockerfile (str): the content of the Dockerfile
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            DockerEnv: the saved DockerEnv instance
        """
        docker_env = DockerEnv(name=name, tag=tag, dockerfile=dockerfile,
                               user_identifier=user_identifier, creation_date=timezone.now(),
                               created_by=user_identifier, modification_date=timezone.now(),
                               modified_by=user_identifier)
        docker_env.save()
        return docker_env


class DockerScriptService:
    """
    Backend methods for the DockerScript django model
    """

    def create_script(self, name, docker_name, docker_tag, script, user_identifier):
        """
        Method to create a new script, associated with a specific DockerEnv

        Args
            | name (str): the file name of the script
            | docker_name (str): the name of the docker container
            | docker_tag (str): the tag of the docker container
            | script (str): the content of the script
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            DockerScript: the registered script
        """
        docker_env_service = DockerEnvService()
        docker_env = docker_env_service.get_unique_docker_env(name=docker_name, tag=docker_tag)
        return self.__save(name=name, docker_env=docker_env, script=script, user_identifier=user_identifier)

    def update_script(self, name, docker_name, docker_tag, script_content, user_identifier):
        """
        Method to update an existing script, associated with a specific DockerEnv

        Args
            | name (str): the file name of the script
            | docker_name (str): the name of the docker container
            | docker_tag (str): the tag of the docker container
            | script_content (str): the content of the script
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            DockerScript: the updated script
        """
        script = self.get_unique_script(docker_name=docker_name, docker_tag=docker_tag, script_name=name)
        script.script = script_content
        script.modification_date = timezone.now()
        script.modified_by = user_identifier
        script.save()
        return script

    def delete_script(self, name, docker_name, docker_tag):
        """
        Method to delete an existing script, associated with a specific DockerEnv

        Args
            | name (str): the file name of the script
            | docker_name (str): the name of the docker container
            | docker_tag (str): the tag of the docker container
        """
        script = self.get_unique_script(docker_name=docker_name, docker_tag=docker_tag, script_name=name)
        script.delete()

    @staticmethod
    def get_unique_script(docker_name, docker_tag, script_name):
        """
        Method to retrieve an existing script, associated with a specific DockerEnv

        Args
            | docker_name (str): the name of the docker container
            | docker_tag (str): the tag of the docker container
            | script_name (str): the file name of the script

        Returns
            DockerScript: the unique DockerScript retrieved from the DB
        """
        docker_env_service = DockerEnvService()
        docker_env = docker_env_service.get_unique_docker_env(name=docker_name, tag=docker_tag)
        return DockerScript.objects.get(docker_env=docker_env, file_name=script_name)

    @staticmethod
    def filter_by_docker_env(docker_env):
        """
        Method to retrieve scripts, associated with a specific DockerEnv

        Args
            docker_env(DockerEnv): the docker container

        Returns
            list: all the scripts associated with the DockerEnv provided by the user
        """
        return DockerScript.objects.filter(docker_env=docker_env)

    @staticmethod
    def __save(name, docker_env, script, user_identifier):
        """
        Private method that saves a DockerScript instance

        Args
            | name (str): the name of the script
            | docker_env(DockerEnv): the associated docker container
            | script (str): the content of the script
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            DockerScript: the saved script of a docker container
        """
        docker_script = DockerScript(file_name=name, docker_env=docker_env, script=script,
                                     creation_date=timezone.now(), created_by=user_identifier,
                                     modification_date=timezone.now(), modified_by=user_identifier)
        docker_script.save()
        return docker_script


class WorkflowService:
    """
    Backend methods for the Workflow django model
    """

    def create_workflow(self, workflow_name, workflow_version, spec_file_name, workflow_file, spec_file, docker_name,
                        docker_tag, user_identifier, workflow_parts=None):
        """
        Method to create a new workflow. If additional workflow part files are provided, they can be registered in the
        workflow_parts table along with the CWL workflow file.

        Args
            | workflow_name (str): the name of the CWL workflow
            | workflow_version (str): the version of the CWL workflow
            | spec_file_name (str): the file name of the associated spec yaml file
            | workflow_file (str): the content of the CWL workflow file
            | spec_file (str): the content of the associated yaml file
            | docker_name (str): the name of the associated docker container
            | docker_tag (str): the tag of the associated docker container
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak
            | workflow_parts (dict): dictionary containing the name, version, content and spec file of a workflow part

        Returns
            Workflow: the registered Workflow instance
        """
        workflow_part_service = WorkflowPartService()
        docker_env_service = DockerEnvService()
        docker_env = docker_env_service.get_unique_docker_env(name=docker_name, tag=docker_tag)
        workflow = self.__save(workflow_name, workflow_version, spec_file_name, workflow_file, spec_file, docker_env,
                               user_identifier)
        if workflow_parts:
            for name, workflow_part in workflow_parts.items():
                spec_name = workflow_part["spec_name"] if "spec_name" in workflow_part.keys() else None
                spec = workflow_part["spec_file"] if "spec_file" in workflow_part.keys() else None
                if spec_name and spec:
                    workflow_part_service.create_workflow_part(name=name, version=workflow_part["version"],
                                                               script=workflow_part["workflow_file"],
                                                               workflow_name=workflow_name,
                                                               workflow_version=workflow_version,
                                                               user_identifier=user_identifier, spec_name=spec_name,
                                                               spec=spec)
                else:
                    workflow_part_service.create_workflow_part(name=name, version=workflow_part["version"],
                                                               script=workflow_part["workflow_file"],
                                                               workflow_name=workflow_name,
                                                               workflow_version=workflow_version,
                                                               user_identifier=user_identifier)
        return workflow

    def update_workflow(self, name, version, workflow_file, spec_file, user_identifier, workflow_fields_update):
        """
        Method to update an existing workflow

        Args
            | name (str): the name of the CWL workflow
            | version (str): the version of the CWL workflow
            | workflow_file (str): the content of the CWL workflow file
            | spec_file (str): the content of the associated yaml file
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak
            | workflow_fields_update(dict): key-value pairs of Workflow fields to be updated

        Returns
            Workflow: the updated Workflow instance
        """
        workflow = self.get_unique_workflow(name=name, version=version)
        if workflow_file:
            workflow_fields_update["workflow"] = workflow_file
        if spec_file:
            workflow_fields_update["spec"] = spec_file
        workflow_fields_update["modification_date"] = timezone.now()
        workflow_fields_update["modified_by"] = user_identifier
        workflow.__dict__.update(workflow_fields_update)
        workflow.save(update_fields=workflow_fields_update.keys())
        return workflow

    def update_docker(self, workflow_name, workflow_version, docker_name, docker_tag, user_identifier):
        """
            Backend method to update the associated docker to a specific workflow

            Args
                | workflow_name (str): the workflow name
                | workflow_version (str): the workflow's version
                | docker_name (str): the name of the registered docker env
                | docker_tag (str): the tag of the registered docker env
                | user_identifier (str): the user identifier (username and issuer)

            Returns
                workflow: the updated workflow object
        """
        docker_service = DockerEnvService()
        docker_env = docker_service.get_unique_docker_env(name=docker_name, tag=docker_tag)
        workflow = self.get_unique_workflow(name=workflow_name, version=workflow_version)
        workflow.user_identifier = user_identifier
        workflow.docker_env = docker_env
        workflow.save()
        return workflow

    def delete_workflow(self, name, version):
        """
        Method to delete a workflow.

        Args
            | name (str): the name of the CWL workflow
            | version (str): the version of the CWL workflow
        """
        workflow = self.get_unique_workflow(name=name, version=version)
        workflow.delete()

    def download_workflow(self, name, version, dockerized=None):
        """
        Method to download a workflow. In the zip file, there are included the associated workflow part files. If the
        request specifies to download the dockerized version, in the zip file there will also be the Dockerfile and
        the additional docker scripts if they exist.

        Args
            | name (str): the name of the CWL workflow
            | version (str): the version of the CWL workflow
            | dockerized (bool): parameter provided **only** if the docker container's files should be included,
            otherwise the value should be null

        Returns
            bytes: the files for download
        """
        workflow_part_service = WorkflowPartService()
        workflow = self.get_unique_workflow(name=name, version=version)
        env_files = {workflow.name: workflow.workflow, workflow.spec_name: workflow.spec}
        workflow_parts = workflow_part_service.filter_by_workflow(name=name, version=version)
        if workflow_parts:
            for workflow_part in workflow_parts:
                env_files[workflow_part.name] = workflow_part.script
                env_files[workflow_part.spec_name] = workflow_part.spec
        if dockerized:
            docker_env = workflow.docker_env
            docker_script_service = DockerScriptService()
            scripts = docker_script_service.filter_by_docker_env(docker_env=docker_env)
            env_files["Dockerfile"] = docker_env.dockerfile
            if scripts:
                for script in scripts:
                    env_files[script.file_name] = script.script
        return utils.get_files(env_files)

    @staticmethod
    def get_unique_workflow(name, version):
        """
        Method to retrieve a unique workflow.

        Args
            | name (str): the name of the workflow
            | version (str): the version of the workflow

        Returns
            Workflow: the retrieved workflow
        """
        return Workflow.objects.get(name=name, version=version)

    @staticmethod
    def __save(workflow_name, workflow_version, spec_file_name, workflow_file, spec_file, docker_env, user_identifier):
        """
        Private method used to save a Workflow instance.

        Args
            | workflow_name (str): the name of the CWL workflow
            | workflow_version (str): the version of the CWL workflow
            | spec_file_name (str): the file name of the associated spec yaml file
            | workflow_file (str): the content of the CWL workflow file
            | spec_file (str): the content of the associated yaml file
            | docker_env(DockerEnv): the docker container
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            DockerEnv: the saved DockerEnv instance
        """
        workflow = Workflow(name=workflow_name, version=workflow_version, workflow=workflow_file,
                            spec_name=spec_file_name, spec=spec_file, docker_env=docker_env,
                            user_identifier=user_identifier, creation_date=timezone.now(), created_by=user_identifier,
                            modification_date=timezone.now(), modified_by=user_identifier)

        workflow.save()
        return workflow


class WorkflowPartService:
    """
    Backend methods for the WorkflowPart django model
    """

    def create_workflow_part(self, name, version, script, workflow_name, workflow_version,
                             user_identifier, spec_name=None, spec=None):
        """
        Method to create a new workflow part object

        Args
            | name (str): the name of the workflow part
            | version (str): the version of the workflow part
            | spec_name (str): the file name of the associated spec yaml file
            | script (str): the content of the CWL file
            | spec (str): the content of the yaml file
            | workflow_name (str): the name of the associated workflow
            | workflow_version (str): the version of the associated workflow
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            WorkflowPart: the created WorkflowPart instance
        """
        workflow_service = WorkflowService()
        workflow = workflow_service.get_unique_workflow(name=workflow_name, version=workflow_version)
        if spec_name and spec:
            return self.__save(name=name, version=version, workflow=workflow, script=script,
                               user_identifier=user_identifier, spec_name=spec_name, spec=spec)
        else:
            return self.__save(name=name, version=version, workflow=workflow, script=script,
                               user_identifier=user_identifier)

    def update_workflow_part(self, workflow_name, workflow_version, workflow_part_name, workflow_part_version, update,
                             user_identifier, workflow_file=None, spec_file=None):
        """
        Method to update an existing workflow part object

        Args
            | workflow_name (str): the name of the associated workflow
            | workflow_version (str): the version of the associated workflow
            | workflow_part_name (str): the name of the workflow part
            | workflow_part_version (str): the version of the workflow part
            | update (dict): key-value pairs of fields to be updated
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak
            | workflow_file(str): the content of the CWL file
            | spec_file (str): the content of the yaml file

        Returns
            WorkflowPart: the created WorkflowPart instance
        """
        workflow_part = self.get_unique_workflow_part(workflow_name=workflow_name, workflow_version=workflow_version,
                                                      workflow_part_name=workflow_part_name,
                                                      workflow_part_version=workflow_part_version)
        if workflow_file:
            update["script"] = workflow_file
        if spec_file:
            update["spec"] = spec_file
        update["modification_date"] = timezone.now()
        update["modified_by"] = user_identifier
        workflow_part.__dict__.update(update)
        workflow_part.save(update_fields=update.keys())
        return workflow_part

    def delete_workflow_part(self, workflow_name, workflow_version, workflow_part_name, workflow_part_version):
        """
        Method to delete an existing workflow

        Args
            | workflow_name (str): the name of the associated workflow
            | workflow_version (str): the version of the associated workflow
            | workflow_part_name (str): the name of the workflow part
            | workflow_part_version (str): the version of the workflow part
        """
        workflow_part = self.get_unique_workflow_part(workflow_name=workflow_name, workflow_version=workflow_version,
                                                      workflow_part_name=workflow_part_name,
                                                      workflow_part_version=workflow_part_version)
        workflow_part.delete()

    def download_workflow_part(self, workflow_name, workflow_version, workflow_part_name, workflow_part_version):
        """
        Method to download an existing workflow

        Args
            | workflow_name (str): the name of the associated workflow
            | workflow_version (str): the version of the associated workflow
            | workflow_part_name (str): the name of the workflow part
            | workflow_part_version (str): the version of the workflow part

        Returns
            bytes: the content to be downloaded
        """
        workflow_part = self.get_unique_workflow_part(workflow_name=workflow_name, workflow_version=workflow_version,
                                                      workflow_part_name=workflow_part_name,
                                                      workflow_part_version=workflow_part_version)
        if workflow_part.spec_name and workflow_part.spec:
            env_files = {workflow_part.name: workflow_part.script, workflow_part.spec_name: workflow_part.spec}
        else:
            env_files = {workflow_part.name: workflow_part.script}
        return utils.get_files(env_files)

    @staticmethod
    def filter_by_workflow(name, version):
        """
        Method to retrieve all parts based on the parent CWL workflow

        Args
            | name (str): the name of the workflow
            | version (str): the version of the workflow

        Returns
            list: all the retrieved WorkflowPart objects associated with a Workflow
        """
        workflow_service = WorkflowService()
        workflow = workflow_service.get_unique_workflow(name=name, version=version)
        return WorkflowPart.objects.filter(workflow=workflow)

    @staticmethod
    def get_unique_workflow_part(workflow_name, workflow_version, workflow_part_name, workflow_part_version):
        """
        Method to retrieve a unique WorkflowPart instance

        Args
            | workflow_name (str): the name of the associated workflow
            | workflow_version (str): the version of the associated workflow
            | workflow_part_name (str): the name of the workflow part
            | workflow_part_version (str): the version of the workflow part

        Returns
            WorkflowPart: the retrieved WorkflowPart object
        """
        workflow_service = WorkflowService()
        workflow = workflow_service.get_unique_workflow(name=workflow_name, version=workflow_version)
        return WorkflowPart.objects.get(name=workflow_part_name, version=workflow_part_version, workflow=workflow)

    @staticmethod
    def __save(name, version, workflow, script, user_identifier, spec_name=None, spec=None):
        """
        Private method to save a workflow part instance

        Args
            | name (str): the name of the workflow part
            | version (str): the version of the workflow part
            | spec_name (str): the file name of the associated spec yaml file
            | workflow (Workflow): the associated workflow object
            | script (str): the content of the CWL file
            | spec (str): the content of the yaml file
            | user_identifier (str): a string containing the username and the Identity Provider of Keycloak

        Returns
            WorkflowPart: the saved WorkflowPart instance
        """
        if spec and spec_name:
            workflow_script = WorkflowPart(name=name, version=version, spec_name=spec_name, workflow=workflow,
                                           script=script, spec=spec, creation_date=timezone.now(),
                                           created_by=user_identifier, modification_date=timezone.now(),
                                           modified_by=user_identifier)
        else:
            workflow_script = WorkflowPart(name=name, version=version, workflow=workflow, script=script,
                                           creation_date=timezone.now(), created_by=user_identifier,
                                           modification_date=timezone.now(), modified_by=user_identifier)
        workflow_script.save()
        return workflow_script
