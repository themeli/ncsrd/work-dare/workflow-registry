from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from workflow_reg.views import *
from workflow_registry import settings

schema_view = get_swagger_view(title='Workflow Registry')

router = routers.DefaultRouter()
router.register(r'docker', DockerEnvView, basename="docker")
router.register(r'scripts', DockerScriptView, basename="scripts")
router.register(r'workflows', WorkflowView, basename="workflows")
router.register(r'workflow_parts', WorkflowPartView, basename="workflow_parts")
router.register(r'accounts', LoginView, basename="accounts")

urlpatterns = [
                  url(r'^docs/$', schema_view),
                  url(r'^admin/', admin.site.urls),
                  url(r'^', include(router.urls))
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

admin.site.site_header = 'Workflow Registry Admin'
admin.site.site_title = "Workflow Registry"
