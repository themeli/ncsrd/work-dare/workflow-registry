import json
from os.path import join, exists, dirname, realpath, abspath
from os import getcwd, listdir, environ, pardir, mkdir
from django.conf import settings


def load_properties():
    dir_path = dirname(realpath(__file__))
    properties_file_path = join(dir_path, "properties.json") if exists(join(dir_path, "properties.json")) else \
        join(dir_path, "example_properties.json")
    with open(properties_file_path) as f:
        return json.loads(f.read())


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = abspath(join(dirname(realpath(__file__)), pardir))
all_properties = load_properties()
properties = all_properties["settings"]
SECRET_KEY = properties["SECRET_KEY"] if properties[
    "SECRET_KEY"] else "8b1+3b*41xrqoc=!v4x1c=h6=2du01s^%r4x_s5*mrwcr(ci_3"

database_to_use = properties["use_database"]
database = properties["DATABASES"][database_to_use]

if database:
    try:
        database["HOST"] = environ["WORKFLOW_REGISTRY_DB_SERVICE_HOST"]
        database["PORT"] = environ["WORKFLOW_REGISTRY_DB_SERVICE_PORT"]
    except (KeyError, Exception):
        pass

DATABASES = {
    'default': {
        'ENGINE': database["ENGINE"],
        'NAME': database["NAME"],
        'USER': database["USER"],
        'PASSWORD': database["PASSWORD"],
        'HOST': database["HOST"],
        'PORT': database["PORT"],
        'OPTIONS': database["OPTIONS"]
    }
}

# Define auth backend
host = environ["DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in environ else "localhost"
port = environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in environ else 5001

if host == "localhost":
    DARE_LOGIN_URL = "https://platform.dare.scai.fraunhofer.de/dare-login"
else:
    DARE_LOGIN_URL = "http://{}:{}".format(host, port)
DEBUG = properties['DEBUG']

ALLOWED_HOSTS = properties["ALLOWED_HOSTS"]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_swagger',
    'workflow_reg.apps.WorkflowRegConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'workflow_registry.urls'

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.RemoteUserBackend',
    'django.contrib.auth.backends.ModelBackend',
    'workflow_reg.auth.DareAuthBackend'
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {  # Adding this section should work around the issue.
                'staticfiles': 'django.templatetags.static',
            },
        },
    },
]

WSGI_APPLICATION = 'workflow_registry.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, "static")
STATICFILES_DIRS = [join(BASE_DIR, 'static_files')]

TOKEN_TIMEOUT = 15  # timeout in minutes

SWAGGER_SETTINGS = {
    "exclude_namespaces": [],  # List URL namespaces to ignore
    "api_version": '0.1',  # Specify your API's version
    "api_path": "/",  # Specify the path to your API not a root level
    "enabled_methods": [  # Specify which methods to enable in Swagger UI
        'get',
        'post',
        'put',
        'delete'
    ],
    "is_authenticated": False,  # Set to True to enforce user authentication,
    "is_superuser": False,  # Set to True to enforce admin only access
    "permission_denied_handler": None,  # If user has no permisssion, raise 403 error
    "permission_classes": []
}

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'PAGINATE_BY': 20,
}

if not exists(join(BASE_DIR, "logs")):
    mkdir(join(BASE_DIR, "logs"))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(BASE_DIR, "logs", "logfile"),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'workflow_registry': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
    }
}
