#! /bin/bash

OUTDIR="output"
chown mpiuser:mpiuser "${OUTDIR}"
cwl-runner --tmp-outdir-prefix=./"${OUTDIR}"/ --tmpdir-prefix=./"${OUTDIR}"/ demo_workflow.cwl spec.yaml