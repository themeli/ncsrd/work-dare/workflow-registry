import json
from os import getcwd
from os.path import join, exists

import requests
import yaml


# ****************************** Login ***************************************
def read_credentials():
    """
        Reads the user's credentials from a yaml file. You should create a file named credentials.yaml in the same
        directory as the workflow_registry_client.py script and your main function or jupyter notebook.

        The yaml file should contain your username, password and the issuer of the token (e.g. Google). If you leave
        the latter field empty, you should be registered in the DARE platform Keycloak module.

        Returns
            | dict: the user's credentials
    """
    cred_file = join(getcwd(), "credentials.yaml")
    example_cred_file = join(getcwd(), "example_credentials.yaml")
    credentials_file = cred_file if exists(cred_file) else example_cred_file
    with open(credentials_file, "r") as f:
        return yaml.safe_load(f.read())


def login(hostname, username, password, requested_issuer):
    """
        User can sign in the DARE platform using this function. You should provide your credentials from the yaml file

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | username (str): your username from the credentials file
            | password (str): your password from the credentials file
            | requested_issuer (str): the Identity Provider (e.g. dare, google etc)

        Returns
            dict: the access and refresh token. If the call failed, it returns an error message
    """
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(r.text)
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"]}
    else:
        print("Could not authenticate user!")


# ***************************** DockerEnv ************************************
def create_docker_env_with_scripts(hostname, token, docker_name, docker_tag, script_names, path_to_files):
    """
        Register an entire docker environment to the DARE platform. Under the path_to_files directory you should already
        have stored a Dockerfile and the relevant bash or python scripts

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_names (list): a list of names for your docker scripts
            | path_to_files (str): the path to the Dockerfile and scripts

        Returns
            tuple: the response status code and the response content
    """
    if not exists(join(path_to_files, "Dockerfile")):
        return "Dockerfile does not exist in folder {}".format(path_to_files)
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_names": script_names,
        "access_token": token
    }
    with open(join(path_to_files, "Dockerfile"), 'r') as d:
        dockerfile = d.read()
    files = {"dockerfile": dockerfile}
    if script_names:
        for script_name in script_names:
            with open(join(path_to_files, script_name), 'r') as s:
                script = s.read()
            files[script_name] = script
    data["files"] = files
    response = requests.post(hostname + "/docker/", json=data)
    return response.status_code, response.text


def create_docker_env(hostname, token, docker_name, docker_tag, docker_path):
    """
        Create a docker environment with a Dockerfile (without any other scripts)

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | docker_path (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile)

        Returns
            tuple: the response status code and the response content
    """
    if not exists(docker_path):
        return "File does not exist"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    with open(docker_path, 'r') as df:
        docker_content = df.read()
    files = {"dockerfile": docker_content}
    data["files"] = files
    response = requests.post(hostname + "/docker/", json=data)
    return response.status_code, response.text


def update_docker(hostname, token, update, docker_name, docker_tag, path_to_docker=None):
    """
        Function to update an existing docker environment. You should provide a dict (parameter update) with the fields
        to be updated. Valid keys to the dict are: name, tag, url, dockerfile

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | update (dict): docker fileds to be updated in the registry
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | path_to_docker (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile) - optional, should be
            | used only if you need to update the Dockerfile

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token,
        "update": update
    }
    if path_to_docker:
        with open(path_to_docker, "r") as df:
            docker_content = df.read()
        files = {"dockerfile": docker_content}
        data["files"] = files
    response = requests.post(hostname + "/docker/update_docker/", json=data)
    return response.status_code, response.text


def provide_url(hostname, token, docker_name, docker_tag, docker_url):
    """
        Function to update the URL to a public docker image for a specific docker environment. The docker env should
        already be available in the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | docker_url (str): the URL to the public docker image

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "docker_url": docker_url,
        "access_token": token
    }
    response = requests.post(hostname + "/docker/provide_url/", data=data)
    return response.status_code, response.text


def delete_docker(hostname, token, docker_name, docker_tag):
    """
        Function to delete a docker environment (Dockerfile and associated scripts)

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    response = requests.delete(hostname + "/docker/delete_docker/", data=data)
    return response.status_code, response.text


def get_docker_by_name_and_tag(hostname, token, docker_name, docker_tag):
    """
        Retrieve a docker environment by name and tag

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    response = requests.get(hostname + "/docker/bynametag", params=data)
    return response.status_code, response.text


def get_docker_by_user(hostname, token, username=None):
    """
        Retrieve a docker env by user. You can specify a user (if it's not you) otherwise the system will identify you
        from the provided token.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | username (str): optional, the username associated with the docker

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/docker/byuser"
    data = {"access_token": token}
    if username:
        data["requested_user"] = username
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_docker(hostname, token, docker_name, docker_tag, local_path):
    """
        Function to download a zip file containing the Dockerfile and the relevant scripts

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/docker/download"
    params = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)


# **************************** Docker Scripts **********************************
def add_script_to_existing_docker(hostname, token, docker_name, docker_tag, script_name, path_to_script):
    """
        Function to register a script (bash or python) to an existing registered docker environment

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script
            | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/add/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    with open(join(path_to_script, script_name), "r") as s:
        script_content = s.read()
    files = {script_name: script_content}
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def edit_script_in_existing_docker(hostname, token, docker_name, docker_tag, script_name, path_to_script):
    """
        Function to update a script (bash or python) to an existing registered docker environment

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script
            | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/edit/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    with open(join(path_to_script, script_name), "r") as s:
        script_content = s.read()
    files = {script_name: script_content}
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def delete_script_in_docker(hostname, token, docker_name, docker_tag, script_name):
    """
        Function to delete a script of a docker env

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
                tuple: the response status code and the response content
    """
    url = hostname + "/scripts/delete/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_script_by_name(hostname, token, docker_name, docker_tag, script_name):
    """
        Function to get a script of a docker env by name

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/byname"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_script(hostname, token, docker_name, docker_tag, script_name, local_path):
    """
        Function to download a script of a docker env by name

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/download"
    params = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)


# ********************************* Workflows *****************************************
def create_workflow(hostname, token, workflow_name, workflow_version, spec_name, path_to_cwls,
                    docker_name, docker_tag, workflow_part_data=None):
    """
        Registers a CWL workflow and its parts (if provided) to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | spec_name (str): the name of the spec yaml file
            | path_to_cwls (str): path to the folder where the CWL files are stored (locally)
            | docker_name (str): the name of the associated docker env
            | docker_tag (str): the tag of the associated docker env
            | workflow_part_data (dict): name,version and yaml file name of each of the CWL of class CommandLineTool

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "spec_file_name": spec_name,
        "workflow_part_data": workflow_part_data,
        "access_token": token
    }
    with open(join(path_to_cwls, workflow_name), "r") as wp:
        workflow_content = wp.read()
    with open(join(path_to_cwls, spec_name), "r") as sp:
        spec_content = sp.read()
    files = {"workflow_file": workflow_content, "spec_file": spec_content}
    if workflow_part_data:
        for workflow_part in workflow_part_data:
            with open(join(path_to_cwls, workflow_part["name"]), "r") as sc:
                script_content = sc.read()
            files[workflow_part["name"]] = script_content
            if "spec_name" in workflow_part.keys():
                with open(join(path_to_cwls, workflow_part["spec_name"]), "r") as sp:
                    spec_content = sp.read()
                files[workflow_part["spec_name"]] = spec_content
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def update_workflow(hostname, token, workflow_name, workflow_version, update, workflow_path=None, specpath=None):
    """
        Updates a CWL workflow to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
            | workflow_path (str): optional, the path to the CWL of class Workflow (only for file update)
            | specpath (str): the path to the yaml file, optional (only if you need to update the file)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/update_workflow/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token,
        "update": update
    }
    if workflow_path:
        with open(workflow_path, "r") as f:
            file_content = f.read()
    if specpath:
        with open(specpath, "r") as sp:
            spec_content = sp.read()
    if file_content or spec_content:
        files = {}
        if file_content:
            files["workflow_file"] = file_content
        if spec_content:
            files["spec_file"] = spec_content
        data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def update_associated_docker(hostname, token, workflow_name, workflow_version, docker_name, docker_tag):
    """
        Function to update the associated docker environment for a specific workflow

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | docker_name (str): the name of the associated docker env
            | docker_tag (str): the tag of the associated docker env

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/update_docker"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token,
        "docker_name": docker_name,
        "docker_tag": docker_tag
    }
    response = requests.post(url, data=data)
    return response.status_code, response.text


def delete_workflow(hostname, token, workflow_name, workflow_version):
    """
        Deletes a CWL workflow and its parts (if provided) from the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/delete_workflow/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_workflow_by_name_and_version(hostname, token, workflow_name, workflow_version):
    """
        Function to retrieve a CWL workflow by name and version

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/bynameversion"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_workflow(hostname, token, workflow_name, workflow_version, local_path, dockerized=None):
    """
        Function to download a CWL workflow (class Workflow) and its CWL parts (class CommandLineTool), the relevant
        yaml files (spec files) and, if dockerized is set to True, it also downloads the Dockerfile and scripts
        (bash or python) of the associated docker container

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | local_path (str): the path where the zip file will be stored
            | dockerized (bool): true to download the Dockerfile and scripts along with the workflow files. Leave it
            empty if you want only the CWL and yaml files

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/download"
    params = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    if dockerized:
        params["dockerized"] = True
    myfile = requests.get(url, params=params)
    if myfile.status_code == 200:
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    else:
        return myfile.status_code, myfile.text


# ******************************* Workflow Parts ********************************
def add_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                      path_to_scripts, spec_name=None):
    """
        Registers a CWL workflow part (class CommandLineTool) to the registry. Relevant yaml file can also be stored,
        if different yaml files are used.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be registered
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be registered
            | path_to_scripts (str): the path to the folder containing the CWL files
            | spec_name (str): optional, use it only if you have different yaml file for each CWL

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/add/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    with open(join(path_to_scripts, workflow_part_name), "r") as s:
        script_content = s.read()
    files = {workflow_part_name: script_content}
    if spec_name:
        data["spec_name"] = spec_name
        with open(join(path_to_scripts, spec_name), "r") as sp:
            spec = sp.read()
        files[spec_name] = spec
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def edit_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                       update, spec_name=None, path_to_files=None):
    """
        Updates a CWL workflow (class CommandLineTool) to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be updated
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be updated
            | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
            | spec_name (str): optional, use it only if you have different yaml file for each CWL
            | path_to_files (str): optional, use it only when you want to update the actual files and not only the
            workflow object's fields

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/edit/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token,
        "update": update
    }

    if path_to_files:
        with open(join(path_to_files, workflow_part_name), "r") as f:
            file_content = f.read()
        files = {workflow_part_name: file_content}
        if spec_name:
            data["spec_name"] = spec_name
            with open(join(path_to_files, spec_name), "r") as sp:
                spec_content = sp.read()
            files[spec_name] = spec_content
        data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def delete_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version):
    """
        Deletes a CWL workflow (class CommandLineTool) from the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be deleted
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be deleted

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/delete/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_workflow_part_by_name_and_version(hostname, token, workflow_name, workflow_version, workflow_part_name,
                                          workflow_part_version):
    """
        Function to retrieve a CWL of class CommandLineTool by name, tag and CWL name & version of the CWL of class
        Workflow

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/bynameversion"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                           local_path):
    """
        Downloads a CWL of class CommandLineTool and its associated yaml file if exists.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved
            | local_path (str): the path to store the zip file / should contain the zip filename as well

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/download"
    params = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)
