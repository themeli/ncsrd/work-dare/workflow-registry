Update db

1. python manage.py makemigrations
2. python manage.py migrate
3. python manage.py migrate --run-syncdb
4. python manage.py createsuperuser
5. python manage.py runserver 
6. python manage.py generateschema > static_file/schema.yml
7. export DJANGO_SETTINGS_MODULE=exec_registry.settings
8. python manage.py collectstatic
9. Automatically create fields from existing db --> python manage.py inspectdb > models.py

Notes

ALTER DATABASE `databasename` CHARACTER SET utf8; 