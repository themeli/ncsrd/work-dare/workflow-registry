.. Workflow-Registry documentation master file, created by
   sphinx-quickstart on Mon Jan 27 13:08:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Workflow-Registry's documentation!
=========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   models
   views
   utils
   tests
   client

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
