Django Models for Workflow Registry
===================================

.. toctree::
        serializers

.. automodule:: workflow_reg.models
   :members: