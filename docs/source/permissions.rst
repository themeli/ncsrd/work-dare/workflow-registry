Permissions for the RESTful API
===============================

.. toctree::
    :caption: Permissions

.. automodule:: workflow_reg.permissions
   :members: