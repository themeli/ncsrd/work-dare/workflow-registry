Testing Workflow-Registry API
=========================

.. toctree::
    :caption: Testing

.. automodule:: workflow_reg.tests
   :members: